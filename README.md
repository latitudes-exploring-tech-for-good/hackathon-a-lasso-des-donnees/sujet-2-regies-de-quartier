# Sujet n°2 : Marchés publics et vie associative - Comité National de Liaison des Régies de Quartiers (https://www.regiedequartier.org/).

+ *Thème :* associations, économie sociale et solidaire, marchés publics. 
+ *Phase d'avancement actuelle :* en production.
+ *Compétences associées :* data science, développement web, design.


##### #0 | Résultats obtenus lors du hackathon "À l'asso des données"
Cette section contiendra des informations sur les résultats obtenus dans le cadre du hackathon "À l'asso des données".

##### #1 | Présentation du Comité National de Liaison des Régies de Quartiers
+ Le réseau des Régies de Quartier et de Territoire regroupe 133 associations, intervenant dans 320 quartiers prioritaires (QPV), et porteuses d’un projet d’économie solidaire tourné autour de trois finalités : le développement économique et l’emploi ; la (re)création du lien social ; le développement de la citoyenneté et du pouvoir d’agir des habitants. 
+ On compte aujourd’hui plus de 8 000 salariés et 2 500 bénévoles dans le réseau, majoritairement habitants des quartiers prioritaires.
+ En moyenne, une Régie réalise plus de 700 000€ de chiffre d’affaires : ils proviennent à 40% de marchés publics obtenus auprès des collectivités (ville principalement) ; et à 40% de marchés publics obtenus auprès des bailleurs sociaux (HLM).

##### #2 | Problématique : rendre visible pour tous les acteurs les contours et les volumes des marchés publics comportant des dispositions sociales et bénéficiant directement aux associations 

Depuis plusieurs décennies, les associations ont diversifié leurs financements. Les marchés publics constituent aujourd’hui une partie importante de leurs ressources. Les associations de l’ESS s’orientent souvent vers des marchés publics comprenant des dispositions sociales. Ces dispositions sociales ne sont pas obligatoires, chaque acheteur public (Etat, collectivités, bailleurs sociaux, établissements publics) fait le choix d’intégrer ou non des clauses sociales. Ces clauses permettent de recruter en priorité des personnes en difficultés ou en situation de handicap, d’attribuer le marché à une association locale accompagnant des personnes éloignées de l’emploi ou fonctionnant en circuit court, d’orienter le marché vers un titulaire de l’économie sociale et solidaire etc. Malgré les annonces et engagements de certains acheteurs publics, la progression attendue des clauses sociales dans les marchés publics n’est pas au rendez-vous : environ 10 % des marchés publics en France comportent une disposition sociale.

Il existe plusieurs observatoires des marchés qui traitent en partie de la question sociale et des associations de l’ESS. Les données disponibles ne permettent pas d’avoir une vision fine de l’existant : elles sont éclatées et tous les marchés ne sont pas comptabilisés. Il n’existe pas une seule forme de « clause sociale » que l’on peut intégrer à un marché mais plusieurs formes de « clauses ». Peu de données sont rendues visibles sur les marchés bénéficiant directement aux associations.

Le plus souvent, la donnée rendue visible est celle du volume d’heure global effectué par des personnes en difficultés (sociales, professionnelles ou handicap) sur un marché. Deux limites à cette donnée :
+ le marché peut être mis en œuvre par une entreprise « classique » ou une association de l’ESS
+ le volume d’heure ne dit rien sur la « qualité de la clause », c’est-à-dire la manière dont les personnes en difficultés sont accompagnées, les associations ne se limitent pas au recrutement de ces personnes mais leur propose un accompagnement spécifique (social, professionnel, de formation). 


##### #3 | Le défi proposé
+ Croiser les données des observatoires/ressources existants pour mieux connaître la réalité des pratiques
+ Mieux faire connaître les acheteurs publics engagés
+ Quelle est la part des associations de l’ESS attributaires de ces marchés ?
+ Quelle est la part des entreprises attributaires de ces marchés situées dans les quartiers prioritaires de la Politique de la Ville ? 
+ Quelle est la part des critères sociaux dans la sélection des offres (critères d’attribution) ? 

##### #4 | Livrables
+ Infographies rendant lisible la méthodologie 
+ Base de données

##### #5 | Ressources à disposition pour résoudre le défi
+ Les données mises à disposition par Etalab : https://www.data.gouv.fr/fr/posts/les-jeux-de-donnees-des-associations/
+ Potentiellement d'ici au hackathon : des données complémentaires de l'OECP (en discussion avec Bercy), l'AdCF, AVE (OK, en attente des données) et du CGET (OK).


##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de stand-up meeting ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Tarek Daher